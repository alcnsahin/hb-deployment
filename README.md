# Hepsiburada Deployment by Ansible

This project provides a Minikube server by ansible. 

#### Ansible performs the following steps sequentially: 

- Creates an EC2 instance on AWS
- Installs and configure a minikube to the server
- Deploys the [hb-web](https://gitlab.com/alcnsahin/hb-web) to the Minikube
- Creates a service
- Autoscales
- Exposes the app
- Shows all resources of the Kubernetes

#### Before to start you need to set some parameters to the `./group_vars/all` file.

<b>region:</b> eu-central-1<br>
<b>instance_type:</b> t2.medium #Minikube needs 2GB of memory at least<br>
<b>ami:</b> ami-0e8286b71b81c3cc1 #centos<br>
<b>group_id:</b> your-aws-ec2-group-id<br>
<b>vpc_subnet_id:</b> your-aws-ec2-subnet-id<br>

#### Required apps & packages:
`pip install -r requirements.txt`<br>
`pip install boto`<br>
`ansible-galaxy collection install amazon.aws`<br>

#### Set AWS public & private Keys as environment
`export AWS_ACCESS_KEY_ID=your_access_key_id`<br>
`export AWS_SECRET_ACCESS_KEY=your_secret_key`
 
 #### Run the app
 `python main.py`