import ansible_runner

print("---- EC2 instance creation job started ----")
ec2_job = ansible_runner.run(private_data_dir='./', playbook='create-ec2-instance.yml', inventory="hosts")
print("{}: {}".format(ec2_job.status, ec2_job.rc))

print("---- Minikube installation job started ----")
minikube_job = ansible_runner.run(private_data_dir='./', playbook='install-minikube.yml', inventory="hosts")
print("{}: {}".format(minikube_job.status, ec2_job.rc))

print("---- K8S Cluster details are getting now ----")
kube_job = ansible_runner.run(private_data_dir='./', playbook='get-k8s-cluster-inf.yml', inventory="hosts")
print("{}: {}".format(kube_job.status, ec2_job.rc))
